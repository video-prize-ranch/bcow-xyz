+++
title = "List of blog posts"
sort_by = "date"
template = "blog.html"
page_template = "blog-page.html"
generate_feeds = true

[extra]
desc = "All of our blog posts."
+++