+++
title = "It's time to ditch Discord"
date = 2021-03-06

[extra]
desc = "When Discord launched back in 2015, it presented itself..."
img = "/img/blog-default.svg"
+++

When Discord launched back in 2015, it presented itself as an alternative to apps like Skype and TeamSpeak with “It's time to ditch Skype and TeamSpeak.”. Discord is widely used in gaming for text and voice chat. At the time, Discord was really compelling. It was completely “free” (free as in price, not as in freedom), unlike Teamspeak. It took the common Silicon Valley approach of not worrying about money until later.

Well 6 years later, it's time to ditch Discord. There are many reasons, from its lack of privacy, censorship, to holding bot developers ransom for IDs.

If you were dumb like me, you messaged friends using Discord, often containing very private information. Anyone at Discord can read your messages. Unlike apps such as Signal, iMessage, or even WhatsApp (not that you should use it), Discord does not have end-to-end encryption meaning that anyone at Discord can read your messages. Attachments uploaded are also completely public in a Google Cloud Storage bucket, anyone with the link (not hard to get) can view and download your attachments. If you've shared sensitive files, delete them.

You're probably saying "But I don't care about privacy!". Using free software alternatives has many benefits. Free software doesn't just mean free in as in price, it also means free as in freedom. You have freedom to change the software and have a say on how it is developed. You can change that little thing that you don't like about your software. Free software also often has a better user experience. Proprietary software isn't designed for you, it is designed to extract as much money from you as possible. Watch [this](https://odysee.com/@AlphaNerd:8/why-you-should-use-free-software:a) video on why you should use free software.

So you've been talking to friends for years on Discord and have sent thousands of messages. All of this information has scared you and now you want to delete your account. You can delete your account, but your messages and attachments will not be deleted. Your name will be changed to Deleted User ******* and your profile picture removed. That's it, all those messages are still there. There's no option to bulk delete messages. You have to delete them one by one or use a script like [this](https://github.com/victornpb/deleteDiscordMessages) but if Discord finds out, you get banned, and you can't delete any more messages.

Discord also collects a lot of data about how you use the service. It makes regular requests to /api/science, a tracking endpoint. These contain your IP address, what you clicked on, and what time. You can view this in your Discord data export, it makes up most of the export.

Now for the part that really makes me want to leave Discord, they want your ID. You should never give your ID to strangers on the internet. If you say anything about being under 13 or even making a joke about it, they will ask for ID. Or if you make a bot and it's in over 100 servers, they want your ID or your bot can't join more servers. This does nothing to stop spammers or data harvesters as those operate using normal user accounts. The only reason Discord would need this type of information is to connect your message content to a real identity.

Now you're convinced and want to leave Discord, thankfully there are great alternatives that respect both your privacy and freedom.

For text chat,

- [Matrix](https://matrix.org/)
- [XMPP](https://xmpp.org/)
- [Signal](https://signal.org/)

For voice/video calls,

- [Mumble](https://www.mumble.info/) (no video calls)
- [Jitsi](https://jitsi.org/) (Matrix uses this for calls)
- [Signal](https://signal.org/)

It's time to get your friends to switch over. This won't be easy, but you'll be forced to anyway when Discord runs out of their investor money. Remember that if your friends truly care about you, they'll switch

Update (May 10, 2021): Discord has added Facebook trackers. DO NOT use the desktop app and only use it in a browser with uBlock Origin.

## Further reading

- [The Future of Bots on Discord | Discord Blog](https://blog.discord.com/the-future-of-bots-on-discord-4e6e050ab52e) (forcing bot devs to provide ID)
- [Why you shouldn't trust Discord | Cadence's Blog](https://cadence.moe/blog/2020-06-06-why-you-shouldnt-trust-discord)
- [Search for "privacy" | Reddit r/discordapp](https://reddit.com/r/discordapp/search/?q=privacy&restrict_sr=on&sort=relevance&t=all) (censoring posts about privacy and labeling them as misinformation)
- [My view on the Issues of Discord | Austin Huang](https://austinhuang.me/discord-issues)
- [Discord - Spyware Watchdog](https://spyware.neocities.org/articles/discord.html)