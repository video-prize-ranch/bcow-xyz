+++
title = "Network-level censorship: The risks and how to safely circumvent it"
date = 2024-04-02

[extra]
desc = "Blocking apps and websites has unintended security and privacy risks for your users."
img = "/img/posts/network-censorship-free-vpns.svg"
+++

![](/img/posts/network-censorship-free-vpns.svg)

Enterprise IT administrators frequently block access to apps and websites to appease management and/or regulators. This has unintended consequences for security that put your network's users in danger of malware and data collection. Blocking content does nothing to prevent its access, users will simply find another way to access it, whether that is by using cellular data or virtual private networks (VPNs).

## "Free" VPNs

Users will always choose the path of least resistance, usually this will be switching to mobile data. However, in cases where this is not available, users will likely go to their app store and search for "free" VPN apps. While some may be legitimate, the vast majority exist to collect and sell user browsing data.

Notably, [Facebook used a VPN to spy on people using Snapchat](https://techcrunch.com/2024/03/26/facebook-secret-project-snooped-snapchat-user-traffic/). Free VPNs still have to make money somehow, and when you aren't paying, that means you're the product. And while everyone is thinking of TikTok collecting data for China, these apps do the same thing, without anyone ever realizing. According to [Business Insider](https://www.businessinsider.com/explainer-here-is-why-you-should-never-use-free-vpn-2020-12), "10 of the most-popular free VPN apps in the US had some form of Chinese ownership".

## Safely circumventing censorship

There are ways to circumvent censorship safely, even for free. The free options will have drawbacks, but low-cost paid options are also available.

> 💡 **Note:** Set up your circumvention tool BEFORE you connect to a censored network. These links are likely to be blocked on a censored network.

### Free options

1. [Proton VPN](https://protonvpn.com/) - Proton is a trustworthy VPN provider with an unlimited free plan.
2. [RiseupVPN](https://riseup.net/en/vpn)/[Calyx VPN](https://calyxos.org/docs/guide/apps/calyx-vpn/) - These VPNs are run by non-profit organizations and are also unlimited (not available for iOS).
3. [Orbot](https://orbot.app/) - Orbot uses the Tor network which can be slow and can cause issues with some apps, not recommended.

### Paid options

Some trustworthy VPNs with censorship circumvention features are [Proton VPN](https://protonvpn.com/) and [Mullvad](https://mullvad.net/). For more, see the [Techlore VPN chart](https://www.techlore.tech/vpn). Another option is mobile data, check with your mobile provider—you may be able to get more data for the same price you are currently paying, and your provider may even have unlimited data options.

## What you can do

1. **Stay safe** - Practice safe censorship circumvention.
2. **Spark change** - Share this with lawmakers, IT administrators, and other decision-makers.
3. **Educate** - Share this article and tell others about the dangers of "free" VPNs and how to safely circumvent censorship.