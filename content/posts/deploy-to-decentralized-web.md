+++
title = "Deploy your site to the decentralized web"
date = 2021-08-16

[extra]
desc = "Hosting static websites on a VPS is very wasteful, expensive, and not scalable."
img = '/img/posts/ipfs.webp'
+++

![](/img/posts/ipfs.webp)

> ⚠️ I can no longer recommend Fleek as they promote privacy invasive services such as Discord and Cloudflare. Their login page now uses Google Firebase, which I have blocked along with the rest of Google.

Hosting static websites on a VPS is very wasteful, expensive, and not scalable. Using static hosting providers such as Cloudflare Pages has privacy implications and allows your site to be censored. IPFS is a peer to peer protocol designed to help decentralize the web, circumventing censorship, and providing real privacy. This article will show you the benefits of the decentralized web and deploy a static website to IPFS.

## What is IPFS?
IPFS is a peer to peer protocol that lets you “pin” files and folders, sharing them on the IPFS network. Viewers also help share the files, like a torrent. Content is addressed using a hash called a CID. Files on IPFS are accessible over HTTP using gateways. For example, [https://ipfs.io/ipfs/bafybeigwwctpv37xdcwacqxvekr6e4kaemqsrv34em6glkbiceo3fcy4si](https://ipfs.io/ipfs/bafybeigwwctpv37xdcwacqxvekr6e4kaemqsrv34em6glkbiceo3fcy4si) and [https://bcow-xyz.ipns.dweb.link/](https://bcow-xyz.ipns.dweb.link/). Today’s web is fragile, expensive, and centralized. The average website only lasts 100 days and countries such as China are blocking access to sites. IPFS helped people in oppressive regimes access Wikipedia, [https://wikipedia-on-ipfs.org/](https://wikipedia-on-ipfs.org/).

## Deploying our site
To deploy our site to IPFS, we will use [Fleek (fleek.co)](https://fleek.co/). If you've used Netlify before, you'll find it very familiar.

### Using GitHub
Fleek only supports deploying from GitHub for now, however you can use their CLI to deploy too.

1. Sign in at [app.fleek.co](https://app.fleek.co/)
2. Click "Add New Site"
3. Connect with GitHub and select your repo
4. Select IPFS and configure your build settings 
5. Deploy site! You can then add a custom domain

> When setting a custom domain, make sure to setup DNSLink by clicking on the 3 dots on your domain and following the instructions. This lets users access your site using other IPFS gateways.

### Using Fleek CLI

1. Install the CLI using npm: `npm install -g @fleekhq/fleek-cli`
2. Login using `fleek login`
3. In your site's directory, run `fleek site:init`
4. To deploy your site, run `fleek site:deploy`

Your site will now be available on IPFS and you can setup a custom domain at [app.fleek.co](https://app.fleek.co/).

> Custom domains require your DNS provider to support ANAME/ALIAS. If yours doesn't, you can `nslookup <random thing>.b-cdn.net` and use the IP address there. This will use the closest BunnyCDN location, you can check where that is by running `whois <IP address>`.