+++
title = "Why you should replace NGINX"
date = 2022-12-10

[extra]
desc = "It's 2022, it's time to upgrade."
img = "/img/why-you-should-replace-nginx.svg"
+++

![](/img/why-you-should-replace-nginx.svg)

A common option for a web server and reverse proxy is NGINX. This would have been a good decision ten years ago, but with modern alternatives built in modern programming languages that have many more features that save you time, why use an inferior product? Simply put, modern web servers like Caddy and Traefik are better.

## Expired TLS certificates
I regularly come across this while browsing the smaller internet; what do they all have in common? NGINX. Most notably, Manjaro appears to struggle with this. Guess which web server they use: NGINX. This would never have happened if they had chosen Caddy (or any other modern web server). Every modern web server supports automatic HTTPS.

## Outdated HTTP protocols
NGINX utilizes HTTP/1.1 by default. Due to missing optimizations present in later versions, this leads to poor website performance. Although HTTP/2 is supported by NGINX, you must enable it (and many users don't). NGINX does not currently support HTTP/3, and even when it does, it is unlikely that it will be enabled by default, and most users will not have access to it. This leads into the next section about NGINX's configuration and their update distribution.

## Bad configuration
NGINX configuration is overly complicated. Many lines of NGINX configuration can be replaced by a few lines of Caddyfile. And because the Caddyfile is a single file, no files need to be moved, and Caddy can reload configuration without restarting.

## Poor update distribution
Linux distributions should not be trusted with updates. Debian and Ubuntu are the most popular server distributions, but they are also the worst at updating (this is a "feature"). This is why packaging formats such as Flatpak exist (in the server world, Docker). Many websites are using NGINX versions that are years old and may contain critical security vulnerabilities (some of this is on the user for not updating even when updates are available). Caddy maintains their own repositories for some Linux distributions, while others, such as Traefik, are designed to run within a container. Developers of these web servers can ship updates quickly and be confident that users will have access to the most recent version.

## Written in C
Every alternative I've mentioned thus far has been written in Go. Go is a modern programming language with memory safety and an excellent HTTP library in its standard library. This makes it the ideal language for creating a web server. Using Go has advantages such as removing dependence on libc and producing a binary that runs on any distro.

## Conclusion
NGINX's antiquated design slows you down and holds back innovation on the web. It's time to move on and replace it with something fresh. Sure, more configuration and software can solve all of these issues. But, at that point, why not just switch? The alternatives are endless, but the best is undoubtedly [Caddy](https://caddyserver.com). It does not have any of the issues I mentioned above and is very easy to use. Traefik (a little difficult to get started with), [Sozu](https://www.sozu.io), and [Envoy](https://www.envoyproxy.io) (I haven't tried these yet, but they look interesting) are some other options. You could also move to a PaaS provider like [Fly.io](https://fly.io/) and stop managing servers altogether, that's what we did here.