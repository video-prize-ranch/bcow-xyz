+++
title = "I regret building Librarian."
date = 2022-08-10

[extra]
desc = "Odysee was a promising YouTube alternative, now it has been ruined."
img = "/img/warning.svg"
+++

> **UPDATE:** Librarian will no longer be maintained. [Learn more >](/posts/archiving-librarian/)

> **EDIT:** The README update to Librarian was a joke. I did not mean to push this and I was not paid to do anything. Sorry for any misunderstanding this may have caused.

Odysee is no longer a good YouTube alternative, it has been ruined by a toxic community. It is [not privacy friendly](https://themarkup.org/blacklight.?url=odysee.com) and the content of the comments section is simply unacceptable. I have lost faith in cryptocurrencies, most are [simply ponzi schemes](https://yt.artemislena.eu/watch?v=0AAUrMuMPlo) and the [incidents](https://web3isgoinggreat.com/) can't be ignored.

I've tried to mitigate these problems. Librarian does not show anything about the LBRY cryptocurrency and there are warnings before viewing comments. I can't keep developing this and pretend that everything will be fine. Odysee clearly won't and can't change, it is tied down by their blockchain technology and stopping the "free speech" marketing and introducing moderation would make them lose most of their user base. Most content on Odysee is synced over from YouTube and then forgotten about, the creators having no idea that such hateful and unacceptable things are being said in their video's comment sections. One of these creators saw what was being said and [took down their content](https://libredd.it/r/notjustbikes/comments/v3thxs/any_idea_what_happened_to_notjustbikes_on_odysee/ib0ech5/?context=3).

Some examples of comments (blurred bad words and redacted names):

![](/img/posts/odysee-comments/1-obfuscated.webp)

![](/img/posts/odysee-comments/2-obfuscated.webp)

These examples were from the creator that removed their content so those comments are gone but it doesn't take long to find more examples.

I have decided to put Librarian in maintenance mode. This means that any outstanding issues about adding features will not be completed. Bugs and security issues will still be fixed. I haven't decided what to do about lbry.bcow.xyz but I am planning to reuse Librarian's code in future frontends.

For alternatives to Odysee and Librarian, you can use [Invidious](https://github.com/iv-org/invidious) or [Piped](https://github.com/TeamPiped/Piped) since most of the content on Odysee is just YouTube content. If you still need or want to use Odysee/LBRY, Librarian will continue to work but there are also desktop apps such as [LBRY-GTK](https://codeberg.org/MorsMortium/LBRY-GTK) and [FastLBRY](https://www.notabug.org/jyamihud/FastLBRY-terminal)
