+++
title = "Skynet - IPFS and web3.storage alternative"
date = 2021-12-05

[extra]
desc = "Skynet is another decentralized storage service like IPFS. Is it better and will I switch?"
img = "/img/blog-default.svg"
+++

> **Update (May 9, 2023):** Skynet has shut down.

> You should probably read my [decentralized web article](/posts/deploy-to-decentralized-web/) first.

I have known about Skynet for over a year but haven't used it due to the extremely unreliable uploads and downloads (which is still true sometimes). Over a year later, the web interface was redesigned and an account system was added. I decided to revisit it and even use it for videos on my site.

[Skynet](https://siasky.net/) lets you upload files to "portals" which are basically IPFS pinning services. The portals don't actually store your files. The portal operator pays someone with Siacoin on the Sia network. This is largely the same as pinning services like Pinata and web3.storage. IPFS does have a blockchain storage marketplace like Sia called Filecoin and it's pretty much the same.

I've found Skynet to be faster than IPFS. The way IPFS works makes finding files on the network very slow (and yes, I have PubSub enabled). Skynet is currently not as decentralized as siasky.net is the only working portal but this is likely to change in the future.

There are some issues with Skynet though, you can't upload folders with files that have special characters like spaces. The siasky.net portal also has some reliability issues, it was down for almost an entire day.

I'm not fully switching to Skynet, but will definitely use it alongside IPFS. I plan to offer the IPFS apps on Skynet soon. This site is currently on Skynet too! Deploying to IPFS without Fleek is very annoying. I'll probably return to using Fleek once I can migrate Matrix homeservers (Fleek CLI doesn't deploy my .well-known).

Skynet has a great article on the differences between IPFS and Skynet. https://docs.siasky.net/developer-guides/moving-from-ipfs-to-skynet