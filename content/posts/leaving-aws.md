+++
title = "Leaving AWS (again)"
date = 2022-04-29

[extra]
desc = "I'm moving away from AWS (again)."
img = "/img/blog-default.svg"
+++

I'm moving away from AWS (again). Before, I hosted many different services but decided to stop because it was unsustainable financially. Now, I develop 2 frontends and needed a place to host official instances, my only choice was AWS. These frontends have been getting more popular and use more than the AWS Free Tier provides, resulting in large bills for data transfer. This month's bill was $10, for 100 GB of data transfer over the 100 GB free tier. To continue to provide these services at a reasonable cost and to minimize time maintaining servers, I have chosen [Render](https://render.com/) and [Fly.io](https://fly.io/) to be our new cloud service providers. Librarian video will continue to be streamed through Amazon CloudFront. Render is currently being tried for both Librarian and rimgo but only offers 100 GB free data transfer and is likely to be temporary.

## Service changes
There may be downtime while services are moved, you can check with our new [status page](https://bcow.instatus.com/). Scribe will be removed due to deployment complexity.

- **Tor hidden services** - No changes
- **Librarian** - No changes
- **rimgo** - No changes
- **Scribe** - Will be removed.