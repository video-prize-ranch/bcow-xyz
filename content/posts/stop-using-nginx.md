+++
title = "Stop using NGINX"
date = 2022-06-18

[extra]
desc = "Hate configuring NGINX and managing TLS certificates? There's a better way."
img = "/img/stop-using-nginx.svg"
+++

![](/img/stop-using-nginx.svg)

> **UPDATE (Nov 13, 2022):** 🚀 Caddy is even better now! HTTP/3 is enabled by default and can provide significant speed improvements. NGINX doesn't even enable HTTP/2 by default and doesn't support HTTP/3 at all.

> **UPDATE (Dec 10, 2022):** See my updated post that goes deeper into this. [Why you should replace NGINX >](/posts/why-you-should-replace-nginx/)

NGINX sucks. This will be controverisal for some but it's true. You probably don't need or want NGINX! It doesn't manage TLS certificates, has hard to understand configuration, and poor documentation. NGINX might have been great a decade ago with "advanced features" but now we have something better.

## Meet Caddy
Caddy is a modern web server written in Go. Caddy can automatically manage your TLS certificates, serve files, and more. It automatically sets up HTTPS using Let's Encrypt. Caddy is very powerful, just like NGINX but is easy to configure with the Caddyfile. Just a few lines of Caddyfile can do so much more than the same in NGINX. Say goodbye to expired certificates and painful configuration.

To install Caddy, follow the instructions [here](https://caddyserver.com/docs/install) and read the [getting started](https://caddyserver.com/docs/getting-started) guide.

## Advanced users: Traefik
If Caddy isn't enough for you, there's Traefik. Traefik is extremely powerful but the configuration can be overwhelming (the docs are great though!). Traefik is also written in Go and automatically sets up HTTPS but is focused on being an _edge router_ instead of a web server. It supports proxying TCP and UDP traffic and is configured dynamically with providers. This makes it great for complex deployments.

To get started, see the documentation [here](https://doc.traefik.io/traefik/).