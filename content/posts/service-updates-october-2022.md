+++
title = "Service updates: October 2022"
date = 2022-10-01

[extra]
desc = "Updates for bcow.xyz services."
img = "/img/blog-default.svg"
+++

We're making a few changes to bcow.xyz services to make your experience better and to reduce our costs and infrastructure.

## Disabling video proxy on Librarian
Video proxying will be disabled on Librarian. This used the Amazon CloudFront CDN to make videos load faster and improve user privacy, but now that Odysee uses a CDN, buffering has been solved. This secondary CDN layer adds latency and increases our costs (with increasing usage, Librarian will be above the free tier soon). Your privacy may also not have been fully protected, as your IP can be leaked through the Via header (this is untested). Some instances such as [lbry.projectsegfau.lt](https://lbry.projectsegfau.lt/) have video proxying with better privacy protection (built into Librarian).

## Less locations?
We run our apps on Fly.io to make deployment easy and this also allows us to easily put our apps in multiple locations around the world. To do this while staying under the free tier, we put both of our apps and Caddy into one "app" (think of it like a container) and put that in 2 locations. But because of this, everything in the container has to share the same 256 MB of memory. This is the reason for the downtime this month, the container would run out of memory and crash. The solution for this is to have a dedicated "app" for each of our apps but with only one location. This way they each get their own 256 MB of memory.

## Discontinuing IPFS apps
The IPFS apps are complicated to deploy and maintain. To allow us to focus on our primary projects (Librarian and rimgo), these will be discontinued. IPFS apps will no longer receive updates and the IPNS names will stop resolving.

## Primary domain for rimgo is changing to rimgo.bcow.xyz
The domain listed on the rimgo instance list and this site will switch to rimgo.bcow.xyz to make it clearer that this is a rimgo instance. i.bcow.xyz will continue to work.