+++
title = "Watch YouTube with RSS and mpv"
date = 2021-05-11

[extra]
desc = "The YouTube website is slow and bloated. They track everything you watch..."
img = "/img/blog-default.svg"
+++

The YouTube website is slow and bloated. They track everything you watch to know more about you. While theres solutions like FreeTube and Invidious, they both have their issues. On your phone, use [NewPipe](https://github.com/TeamNewpipe/NewPipe) (on iOS, Invidious is your only option).

FreeTube is an Electron app, this won't solve any issues with the bloat of the official YouTube site. It will eat your memory but you'll have privacy.

Invidious is unreliable, instances frequently get blocked by YouTube or go down entirely. Invidious can be self-hosted to fix both of these issues but most do not have the technical skill to set that up.

## The better way
RSS and mpv is the best way to watch YouTube. mpv is lightweight and hardware accelerated, your YouTube playback experience will be much better. Unlike FreeTube, GNOME Feeds is a native GTK app, it will look great on your desktop without eating memory. You may be like "how do I search?!", we'll get to that later. This article will focus on a setup with GNOME Feeds but you can use other RSS readers like Liferea and Newsboat.

### Install

You'll need to install Feeds, mpv, and youtube-dl. This tutorial won't work on Windows or Mac. Enter the commands below based on your distro to install the required packages.

- Arch Linux: `sudo pacman -S gfeeds mpv youtube-dl`
- Debian (and Debian-based): `sudo apt install gnome-feeds mpv youtube-dl`
- Fedora: `sudo dnf install gnome-feeds mpv youtube-dl`
- Other distros: [gnome-feeds](https://gfeeds.gabmus.org/), [mpv](https://mpv.io/), [youtube-dl](https://youtube-dl.org/)

### Setup

Once you have all of that installed, you'll need to export your YouTube subscriptions. To do this, go to [takeout.google.com](https://takeout.google.com/), deselect all options and click on YouTube, click on the format "JSON" and change it to OPML. Start the export then refresh the page and at the top, you should be able to download your export. Extract the ZIP file and locate the OPML file.

In Feeds, click on the three dots and click "Import OPML". Select the OPML file from Google Takeout. Feeds will now start to import and refresh your subscriptions. This can take a while if you have a lot of subscriptions.

To get Feeds to open YouTube videos in mpv, click the 3 dots in Feeds and click "Preferences", toggle the "Open YouTube links via your video player" option and type in mpv for the "Preferred media player".

You are now ready to enjoy YouTube using RSS and mpv! You can also configure mpv and add scripts to add features.

### Configuring mpv (optional)

The [mpv manual](https://mpv.io/manual/master/) has everything you need to configure and use mpv. I'll go over my mpv config file in this section.

~/.config/mpv/mpv.conf
```
ytdl-format=248+251/303+251/bestvideo[height<=?1080]+bestaudio/best
hwdec=nvdec
```

ytdl-format tells youtube-dl what quality to download for mpv to play. This configuration means, play 1080p VP9 with Opus audio, if that isn't available, play 1080p60 VP9 with Opus audio, if that isn't available, play the format with the best video and audio quality.

hwdec=nvdec uses NVIDIA NVDEC to decode the video using the graphics card to reduce CPU load.
Searching YouTube

To search YouTube, you can use Invidious or [ytfzf](https://github.com/pystardust/ytfzf).