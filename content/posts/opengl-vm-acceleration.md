+++
title = "Love virtual machines again: setup OpenGL graphics acceleration"
date = 2021-08-24

[extra]
desc = "If you've used virtual machines before, you probably know how slow they are."
img = "/img/blog-default.svg"
+++

If you've used virtual machines before, you probably know how slow they are. This is because there is no hardware acceleration for graphics. Using virgl graphics, you can drag windows around smoothly and play Minecraft and potentially other games with good performance. This will only work for Linux guests.

<div class="grid justify-center">
  <video controls width="480" preload="none" poster="/img/posts/opengl-vm-acceleration/video.avif">
    <source src="https://ipfs.io/ipfs/bafybeic7f5fyo2lqybk57jzhb7kur6rqnkhm3bxpl2s7b76xapx5bdvxra/" type="video/webm">
    <source src="https://ipfs.io/ipfs/bafybeiabtpelubodj25fmut3bmdmpo2jjkzvbtaovtwgxscc7osxot3r7q" type="video/mp4">
  </video>
  <p class="text-center">Playing Minecraft at playable framerates.</p>
</div>

## Intel/AMD graphics
Enabling OpenGL acceleration on Intel or AMD graphics is very easy. If these steps don't work, try the NVIDIA instructions below.

1. In `virt-manager`, change your Video to the model Virtio and tick the box for 3D acceleration.
![](/img/posts/opengl-vm-acceleration/1.webp)

2. Tick the box labeled OpenGL in Display Spice and select your Intel or AMD graphics.
![](/img/posts/opengl-vm-acceleration/2.webp)

> To play games, you will have to remove the Tablet device and add a USB Mouse under Add Hardware > Input. You can also passthrough a second mouse.

## NVIDIA
Enabling OpenGL acceleration on NVIDIA graphics will require you to use the terminal.

```
virt-xml $DOMAIN --edit --confirm --qemu-commandline='-display sdl,gl=on'
virt-xml $DOMAIN --edit --confirm --qemu-commandline='env=DISPLAY=:1'
```

`$DOMAIN` is the name of your virtual machine in `virt-manager`

> You may need to change DISPLAY to :0 if :1 doesn't work. You can use the XML editor in `virt-manager`.

Your virtual machine will appear in a seperate window.

> To play games, you will have to remove the Tablet device and add a USB Mouse under Add Hardware > Input. You can also passthrough a second mouse.