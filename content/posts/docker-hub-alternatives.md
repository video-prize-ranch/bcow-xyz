+++
title = "Docker Hub alternatives: why and what to switch to"
date = 2023-04-02

[extra]
desc = "Don't want to pay for Docker Hub? Try these alternatives."
img = "/img/blog-default.svg"
+++

Docker Hub sucks. It's full of invasive trackers (including one that records everything you do on the site!). And now with Docker Hub deleting free organizations, there's even more reasons to switch.

## Alternatives

Switching registries is easy! And there's many to choose from, you probably don't even need to make a new account. The only difference when pulling images is that you have to add your registry before your tag. For example, example/image:latest would become ghcr.io/example/image:latest on GitHub.

### Software forges

This is the best option for most people, you probably already have an account at one of these to share your code, why not your images too? Every big software forge (GitHub, GitLab, and Forgejo) has a container registry.

- [Codeberg/Gitea/Forgejo](https://docs.gitea.io/en-us/packages/container/)
- [GitLab](https://docs.gitlab.com/ee/user/packages/container_registry/)
- [GitHub](https://docs.github.com/en/packages/working-with-a-github-packages-registry/working-with-the-container-registry)

### Cloud providers
This option can get pricey. The big 3 cloud providers all have container registries (not that you should use them, we're trying to improve our privacy right?).

- [AWS Elastic Container Registry](https://aws.amazon.com/ecr/)
- [Google Cloud Container Registry](https://cloud.google.com/container-registry/)
- [Azure Container Registry](https://learn.microsoft.com/en-us/azure/container-registry/)

### Quay.io

> **Update (2024-02-03):** Quay is now just as bad, if not worse than Docker Hub. Quay contacts many trackers like Adobe, Stripe, Google, and shows an intrustive cookie popup that is likely a tracker itself. Registering for a Red Hat account no longer requires an address but now requires a phone number. Do not use Quay.

[Quay.io](https://quay.io/) isn't my favorite, the UI feels outdated and signing up requires a physical address (!).

### Self-hosted

If you like self-hosting, you can self-host your own container registry. To get started, follow the guide below:
https://docs.docker.com/registry/deploying/

> **Note:** You can also self-host Forgejo or GitLab to get a container registry.

## Podman

[Podman](https://podman.io/) isn't a container registry but I'm mentioning it here anyway. Podman is an alternative to Docker that works almost entirely the same but with better security and without Docker's greed for profits.
